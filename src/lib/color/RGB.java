package lib.color;

public class RGB extends Color {
    
    private int red;
    
    private int green;
    
    private int blue;
    
    public RGB(){
        
    }

    public RGB(int red, int green, int blue) {
        this.red = red;
        this.green = green;
        this.blue = blue;
    }

    public int getRed() {
        return red;
    }

    public void setRed(int red) {
        this.red = red;
    }

    public int getGreen() {
        return green;
    }

    public void setGreen(int green) {
        this.green = green;
    }

    public int getBlue() {
        return blue;
    }

    public void setBlue(int blue) {
        this.blue = blue;
    }

    @Override
    public java.awt.Color toAWTColor() {
        return new java.awt.Color(red, green, blue);
    }

    @Override
    public RGB toRGB() {
        return this;
    }
    
    @Override
    public CMYK toCMYK(){
        CMYK cmyk = new CMYK();
        
        float red = this.red / 255.0f;
        float green = this.green / 255.0f;
        float blue = this.blue / 255.0f;
        float key = 1 - Math.max(red, Math.max(green, blue));
        
        cmyk.setKey(key);
        cmyk.setCyan((1 - red - key) / (1 - key));
        cmyk.setMagenta((1 - green - key) / (1 - key));
        cmyk.setYellow((1 - blue - key) / (1 - key));
        
        return cmyk;
    }
    
    @Override
    public HSV toHSV(){
        HSV hsv = new HSV();
        
        float red = this.red / 255.0f;
        float green = this.green / 255.0f;
        float blue = this.blue / 255.0f;
        
        float cmax = Math.max(red, Math.max(green, blue));
        float cmin = Math.min(red, Math.min(green, blue));
        float delta = cmax - cmin;
        
        int hue = 0;
        if (cmax == red){
            hue = (int)(60 * (((green - blue) / delta) % 6));
        } else if (cmax == green){
            hue = (int)(60 * (((blue - red) / delta) + 2));
        } else if (cmax == blue){
            hue = (int)(60 * (((red - green) / delta) + 4));
        }
        
        if (hue < 0)
            hue += 360;
        hsv.setHue(hue);
        
        float saturation = 0.0f;
        if (cmax != 0){
            saturation = delta / cmax;
        }
        hsv.setSaturation(saturation * 100);
        hsv.setValue(cmax * 100);
        return hsv;
    }

    @Override
    public HSL toHSL() {
        HSL hsl = new HSL();
        
        float red = this.red / 255.0f;
        float green = this.green / 255.0f;
        float blue = this.blue / 255.0f;
        
        float cmax = Math.max(red, Math.max(green, blue));
        float cmin = Math.min(red, Math.min(green, blue));
        float delta = cmax - cmin;
        
        int hue = 0;
        if (cmax == red){
            hue = (int)(60 * (((green - blue) / delta) % 6));
        } else if (cmax == green){
            hue = (int)(60 * (((blue - red) / delta) + 2));
        } else if (cmax == blue){
            hue = (int)(60 * (((red - green) / delta) + 4));
        }
        
        if (hue < 0)
            hue += 360;
        hsl.setHue(hue);
        
        float lightness = (cmax + cmin) / 2;
        float saturation = 0.0f;
        
        if (lightness != 1.0f){
            saturation = delta / (1 - Math.abs(2 * lightness - 1));
        }
        hsl.setSaturation(saturation * 100);
        hsl.setLightness(lightness * 100);
        return hsl;
    }
    
    @Override
    public String toHex(){
        return "#" + String.format("%02X", red) + String.format("%02X", green) + String.format("%02X", blue);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof RGB){
            return ((RGB) obj).getRed() == this.red &&
                   ((RGB) obj).getGreen() == this.green &&
                   ((RGB) obj).getBlue() == this.blue;
        }
        return false;
    }
    
    @Override
    public String toString(){
        if (super.toString() != null){
            return super.toString();
        }
        return "[" + red + ", " + green + ", " + blue + "]";
    }
    
    public static RGB fromHex(String hex){
        if (hex.startsWith("#")){
            hex = hex.substring(1);
        }        
        int r = Integer.parseInt(hex.substring(0, 2), 16);
        int g = Integer.parseInt(hex.substring(2, 4), 16);
        int b = Integer.parseInt(hex.substring(4, 6), 16);
        
        return new RGB(r, g, b);
    }
}
