package lib.color;

public abstract class Color {
    
    private String name;
    
    public Color(){
        
    }

    public Color(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public java.awt.Color toAWTColor(){
        return this.toRGB().toAWTColor();
    }
    
    public abstract RGB toRGB();
    
    public CMYK toCMYK(){
        return this.toRGB().toCMYK();
    }
    
    public HSV toHSV(){
        return this.toRGB().toHSV();
    }
    
    public HSL toHSL(){
        return this.toRGB().toHSL();
    }
    
    public String toHex(){
        return this.toRGB().toHex();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Color){
            return ((Color) obj).getName().equals(this.name);
        }
        return false;
    }    

    @Override
    public String toString() {
        return this.name;
    }
}
