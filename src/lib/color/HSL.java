/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.color;

import java.util.Locale;

/**
 *
 * @author thomas
 */
public class HSL extends Color {
    
    private int hue;
    
    private float saturation;
    
    private float lightness;
    
    public HSL() {
    
    }

    public HSL(int hue, float saturation, float lightness) {
        this.hue = hue;
        this.saturation = saturation;
        this.lightness = lightness;
    }

    public int getHue() {
        return hue;
    }

    public void setHue(int hue) {
        this.hue = hue;
    }

    public float getSaturation() {
        return saturation;
    }

    public void setSaturation(float saturation) {
        this.saturation = saturation;
    }

    public float getLightness() {
        return lightness;
    }

    public void setLightness(float lightness) {
        this.lightness = lightness;
    }

    @Override
    public RGB toRGB() {
        RGB rgb = new RGB();
        
        float saturation = this.saturation / 100;
        float lightness = this.lightness / 100;        
        
        float c = (1 - Math.abs(2 * lightness - 1)) * saturation;
        float x = c * (1 - Math.abs(((float)hue / 60) % 2 - 1));
        float m = lightness - c / 2;
        
        float r = 0, g = 0, b = 0;
        
        if (hue >= 0 && hue < 60){
            r = c;
            g = x;
        } else if (hue >= 60 && hue < 120){
            r = x;
            g = c;
        } else if (hue >= 120 && hue < 180){
            g = c;
            b = x;
        } else if (hue >= 180 && hue < 240){
            g = x;
            b = c;
        } else if (hue >= 240 && hue < 300){
            r = x;
            b = c;
        } else if (hue >= 300 && hue < 360){
            r = c;
            b = x;
        }
        rgb.setRed((int)((r + m) * 255));
        rgb.setGreen((int)((g + m) * 255));
        rgb.setBlue((int)((b + m) * 255));
        return rgb;
    }

    @Override
    public HSL toHSL() {
        return this;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof HSL){
            return ((HSL) obj).getHue() == this.hue &&
                   ((HSL) obj).getLightness() == this.lightness &&
                   ((HSL) obj).getSaturation() == this.saturation;
        }
        return false;
    }

    @Override
    public String toString() {
        if (super.toString() != null){
            return super.toString();
        }
        return "[" + hue + ", " + String.format(Locale.US, "%.2f", saturation) + ", " + String.format(Locale.US, "%.2f", lightness) + "]";
    }
}
