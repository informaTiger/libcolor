package lib.color;

import java.util.Locale;

public class CMYK extends Color {
    
    private float cyan;
    
    private float magenta;
    
    private float yellow;
    
    private float key;
    
    public CMYK(){
        
    }

    public CMYK(float cyan, float magenta, float yellow, float key) {
        this.cyan = cyan;
        this.magenta = magenta;
        this.yellow = yellow;
        this.key = key;
    }

    public float getCyan() {
        return cyan;
    }

    public void setCyan(float cyan) {
        this.cyan = cyan;
    }

    public float getMagenta() {
        return magenta;
    }

    public void setMagenta(float magenta) {
        this.magenta = magenta;
    }

    public float getYellow() {
        return yellow;
    }

    public void setYellow(float yellow) {
        this.yellow = yellow;
    }

    public float getKey() {
        return key;
    }

    public void setKey(float key) {
        this.key = key;
    }
    
    @Override
    public RGB toRGB(){
        RGB rgb = new RGB();
        
        rgb.setRed((int)(255 * (1 - cyan) * (1 - key)));
        rgb.setGreen((int)(255 * (1 - magenta) * (1 - key)));
        rgb.setBlue((int)(255 * (1 - yellow) * (1 - key)));
        
        return rgb;
    }

    @Override
    public CMYK toCMYK() {
        return this;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof CMYK){
            return ((CMYK) obj).getCyan() == this.cyan &&
                   ((CMYK) obj).getMagenta() == this.magenta &&
                   ((CMYK) obj).getYellow() == this.yellow &&
                   ((CMYK) obj).getKey() == this.key;
        }
        return false;
    }
    
    @Override
    public String toString(){
        if (super.toString() != null){
            super.toString();
        }
        return "[" + String.format(Locale.US, "%.2f", cyan) + ", " + String.format(Locale.US, "%.2f", magenta) + ", " + String.format(Locale.US, "%.2f", yellow) + ", " + String.format(Locale.US, "%.2f", key) + "]";
    }
}