package lib.color;

import java.util.Locale;

public class HSV extends Color {
    
    private int hue;
    
    private float saturation;
    
    private float value;
    
    public HSV(){
        
    }

    public HSV(int hue, float saturation, float value) {
        this.hue = hue;
        this.saturation = saturation;
        this.value = value;
    }

    public int getHue() {
        return hue;
    }

    public void setHue(int hue) {
        this.hue = hue;
    }

    public float getSaturation() {
        return saturation;
    }

    public void setSaturation(float saturation) {
        this.saturation = saturation;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    @Override
    public java.awt.Color toAWTColor() {
        return this.toRGB().toAWTColor();
    }
    
    @Override
    public RGB toRGB(){
        RGB rgb = new RGB();
        
        float saturation = this.saturation / 100;
        float value = this.value / 100;
        
        float c = value * saturation;
        float x = c * (1 - Math.abs((((float)hue / 60) % 2) - 1));
        float m = value - c;
        
        float r = 0, g = 0, b = 0;
        
        if (hue >= 0 && hue < 60){
            r = c;
            g = x;
        } else if (hue >= 60 && hue < 120){
            r = x;
            g = c;
        } else if (hue >= 120 && hue < 180){
            g = c;
            b = x;
        } else if (hue >= 180 && hue < 240){
            g = x;
            b = c;
        } else if (hue >= 240 && hue < 300){
            r = x;
            b = c;
        } else if (hue >= 300 && hue < 360){
            r = c;
            b = x;
        }
        rgb.setRed((int)((r + m) * 255));
        rgb.setGreen((int)((g + m) * 255));
        rgb.setBlue((int)((b + m) * 255));
        return rgb;
    }

    @Override
    public HSV toHSV() {
        return this;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof HSV){
            return ((HSV) obj).getHue() == this.hue &&
                   ((HSV) obj).getSaturation() == this.saturation &&
                   ((HSV) obj).getValue() == this.value;
        }
        return false;
    }
    
    @Override
    public String toString(){
        if (super.toString() != null){
            return super.toString();
        }
        return "[" + hue + ", " + String.format(Locale.US, "%.2f", saturation) + ", " + String.format(Locale.US, "%.2f", value) + "]";
    }
}
